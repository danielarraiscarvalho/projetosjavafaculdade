
public class Teste {

    public static void main(String args[]) {
        HotelDuplo vrHotel = new HotelDuplo();
        Hospede vrHospede1 = new Hospede("Ana", "406 norte",
                "82165342098");
        Hospede vrHospede2 = new Hospede("Pedro", "206 sul",
                "44354738937");
        System.out.println(vrHotel.verificaLotacao());
        vrHotel.reservaQuartoSolteiro(vrHospede1);
        vrHotel.reservaQuartoCasal(vrHospede2);
        System.out.println(vrHotel.verificaLotacao());
        System.out.println("");
        vrHotel.liberaQuartoSolteiro();
        
        System.out.println(vrHotel.verificaLotacao());
    }
}
