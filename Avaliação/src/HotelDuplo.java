
public class HotelDuplo {

    Quarto vrQuarto1;
    Quarto vrQuarto2;

    public HotelDuplo() {
        vrQuarto1 = new Quarto(1, Quarto.QUARTO_CASAL, true);
        vrQuarto2 = new Quarto(2, Quarto.QUARTO_SOLTEIRO, false);
    }
    
    public void reservaQuartoSolteiro(Hospede Hospede) {
          vrQuarto1.setOcupado(Hospede);
    }

    public void reservaQuartoCasal(Hospede Hospede) {
        vrQuarto2.setOcupado(Hospede);
    }

    public void liberaQuartoSolteiro() {
        vrQuarto1.setOcupado(null);
    }

    public void liberaQuartoCasal() {
        vrQuarto1.setOcupado(null);
    }

    public boolean verificaLotacao() {
        if (vrQuarto1.isOcupado() && vrQuarto2.isOcupado()) {
            return true;
        }else {
            return false;
        }
    }

}
