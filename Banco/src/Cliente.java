public class Cliente {
    private String Nome;
    private int Idade;
    private String endereco;
    private Conta Conta;

    public String getNome() {
        return Nome;
    }

    public void setNome(String Nome) {
        this.Nome = Nome;
    }

    public int getIdade() {
        return Idade;
    }

    public void setIdade(int Idade) {
        this.Idade = Idade;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public Conta getContas() {
        return Conta;
    }

    public void setConta(int numero, String tipo) {
        Conta = new Conta(Nome, numero, tipo);
    }

    public Cliente(String Nome, int Idade, String endereco, int numeroConta, String tipo ) {
        this.Nome = Nome;
        this.Idade = Idade;
        this.endereco = endereco;
        this.Conta = new Conta(Nome, numeroConta, tipo);
    }
    
    
    
    
}
