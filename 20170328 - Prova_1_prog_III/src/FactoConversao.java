//Pacotes utilizados
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class FactoConversao extends JFrame implements ActionListener
{
	//Variaveis de referencia da classe
	private JTextField vrCampoValorReal = null;
	private JPanel vrPainelCentral = null;
	private JRadioButton vrDolar = null;
	private JRadioButton vrEuro = null;
	private ButtonGroup grupo = null;
	private JPanel vrPainelSul = null;
	private JButton vrBotaoCalcular = null;
	private JLabel vrResultado = null;
	
	//Construtor da classe
	public FactoConversao()
	{
		//Configuracoes de janela
		setSize(350,280);
		setTitle("FactoConversao");
		setResizable(false);
		BorderLayout layout = new BorderLayout();
		layout.setVgap(50);
		setLayout(layout);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//Executa o metodo para montar a interface
		montaInterface();
		
		//Exibe a janela
		setVisible(true);
	}
	
	//Monta os componentes de interface
	private void montaInterface()
	{
		//Monta o display
		vrCampoValorReal = new JTextField(10);
		vrCampoValorReal.setFont(new Font("Arial", Font.BOLD, 20));
		vrCampoValorReal.setForeground(Color.blue);
		vrCampoValorReal.setHorizontalAlignment(SwingConstants.CENTER);
		add(vrCampoValorReal, BorderLayout.NORTH);
		
		//Monta o painel central contendo os radio buttons
		vrDolar = new JRadioButton("D�lar");
		vrEuro = new JRadioButton("Euro");
		grupo = new ButtonGroup();
		grupo.add(vrDolar);
		grupo.add(vrEuro);
		vrDolar.setSelected(true);
		vrPainelCentral = new JPanel();
		vrPainelCentral.setLayout(new FlowLayout());
		vrPainelCentral.add(vrDolar);
		vrPainelCentral.add(vrEuro);
		add(vrPainelCentral, BorderLayout.CENTER);
		
		//Monta o painal sul contando os botoes de controle
		vrBotaoCalcular = new JButton("Converter");
		vrBotaoCalcular.addActionListener(this);
		vrResultado = new JLabel("Resultado:");
		vrPainelSul = new JPanel();
		vrPainelSul.setLayout(new FlowLayout());
		vrPainelSul.add(vrBotaoCalcular);
		vrPainelSul.add(vrResultado);
		add(vrPainelSul,BorderLayout.SOUTH);
	}
	
	//Metodo principal de um programa Java
	public static void main(String...args)
	{
		new FactoConversao();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			float valor = tratarFloat(vrCampoValorReal.getText());
			valor = convert(valor);
			vrResultado.setText("Resultado: "+ ((vrDolar.isSelected()) ? "UR$ " : "� ")+new DecimalFormat("#.00").format(valor));	
		} catch (Exception e2) {
			vrResultado.setText("Valor informado Inv�lido");
		}
	}
	
	public float convert(float valor){
		float dollar = (float) (1/3.14f);
		float euro =(float)  (1/3.4f);
		if (vrDolar.isSelected()) {
			valor = (float) (dollar*valor);			
		}else if (vrEuro.isSelected()) {
			valor = (float) (valor*euro);	
		}
		return valor;
	}
	public float tratarFloat(String floate){
		if (floate.contains(",")) {
			floate = floate.replace(",", ".");
			return Float.valueOf(floate);
		}
		return Float.valueOf(floate);
	}
}
