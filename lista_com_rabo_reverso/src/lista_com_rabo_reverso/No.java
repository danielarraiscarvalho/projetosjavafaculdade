package lista_com_rabo_reverso;

public class No<T>  {
	private T dado;
    private No proximo;
    private No anterior;

    public T getDado() {
        return dado;
    }

    public void setDado(T dado) {
        this.dado = dado;
    }

    public No getProximo() {
        return proximo;
    }

    public void setProximo(No proximo) {
        this.proximo = proximo;
    }
    
    public No getAnterior() {
		return anterior;
	}

	public void setAnterior(No anterior) {
		this.anterior = anterior;
	}

	public No(T dado) {
        setDado(dado);
        setProximo(null);
        setAnterior(null);
    }
}
