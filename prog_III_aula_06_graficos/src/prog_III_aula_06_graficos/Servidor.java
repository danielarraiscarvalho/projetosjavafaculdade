package prog_III_aula_06_graficos;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JFrame;

public class Servidor extends JFrame implements Runnable {
	/**
	*
	*/
	private static final long serialVersionUID = 1L;
	public Map<String, Color> mapaCores = new HashMap<String, Color>();
	
	int x = 0, y = 0;
	Color cor = null;
	ServerSocket servidor = null;
	Socket cliente = null;
	DataInputStream canalEntrada = null;
	int tamanho = 0;
	
	public void run() {
		String msg = "";
		while (true) {
			try {
				msg = canalEntrada.readUTF();
				String sX = msg.split(";")[0];
				String sY = msg.split(";")[1];
				//cor = mapaCores.get(msg.split(";")[2]);
				x = Integer.valueOf(sX);
				y = Integer.valueOf(sY);
				//tamanho = Integer.valueOf(msg.split(";")[3]);
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			repaint();
		}
	}

	Servidor() {
		setSize(600, 600);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Servidor Aguardando...");
		setLayout(new FlowLayout());
		mapaCores.put("Vermelho", Color.red);
		mapaCores.put("Verde", Color.green);
		mapaCores.put("Preto", Color.black);
		mapaCores.put( "Azul", Color.blue);
		setVisible(true);

		try {
			servidor = new ServerSocket(3067);
			cliente = servidor.accept();
			setTitle("Servidor Conectado!");
			canalEntrada = new DataInputStream(cliente.getInputStream());
			new Thread(this).start();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void paint(Graphics g) {
		g.setColor(Color.red);
		g.fillArc(x, y, 10, 10, 0, 360);
	}

	public void pausa() {
		try {
			Thread.sleep(100);
		} catch (Exception e) {

		}
	}

	public static void main(String... args) {
		new Servidor();
	}
}