
import java.util.Scanner;

public class Atv2_For {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int v;
        System.out.print("Informe um valor qualquer maior que 1: ");
        v = s.nextInt();
        for (int i = 1; i <= v; i++) {
            System.out.println(i);
        }
    }
}
