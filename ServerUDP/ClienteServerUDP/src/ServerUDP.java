import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class ServerUDP {
	public static void main(String[] argv) throws Exception {
		DatagramSocket datagramSocket = new DatagramSocket();
		byte[] buffer = "0123456789".getBytes();
		InetAddress receiverAddress = InetAddress.getLocalHost();
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length, receiverAddress, 80);
		while (true) {
			datagramSocket.send(packet);
		}
	}
}