package Models;

import arrayList.MeuArray;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class GerenteArmazenamento {

    private static GerenteArmazenamento psGArmazenamento = null;

    private GerenteArmazenamento() {
    }

    public static GerenteArmazenamento retornaPSArmazenamento() {
        if (psGArmazenamento == null) {
            psGArmazenamento = new GerenteArmazenamento();
        }
        return psGArmazenamento;
    }

    //Método respossável pelo registro de cliente.
    public static void gravaCliente(Cliente c) throws IOException {
        String url = "src\\dados.txt";
        BufferedWriter gravarArq = new BufferedWriter(new FileWriter(url, true));
        gravarArq.write(c.getNome() + "\n");
        gravarArq.write(c.getEndereco() + "\n");
        gravarArq.write(c.getIdade() + "\n");
        gravarArq.write(c.getSexo() + "\n");
        gravarArq.close();
    }

    //Método que retorna os cliente cadastrados
    public static MeuArray<Cliente> buscarCliente() throws FileNotFoundException {

        String url = "src\\dados.txt";
        Scanner s = new Scanner(new FileReader(url));
        MeuArray<Cliente> clientes = new MeuArray<>();
        while (s.hasNextLine()) {
            String nome = s.nextLine();
            String endereco = s.nextLine();
            int idade = Integer.valueOf(s.nextLine());
            char[] sexo = s.nextLine().toCharArray();
            clientes.add(new Cliente(nome, idade, endereco, sexo[0]));
        }
        s.close();

        return clientes;
    }

    public static void removerCliente(Cliente c) throws IOException {
        MeuArray<Cliente> clientes = buscarCliente();
        String url = "src\\dados.txt";
        
        //um buferred sem true para append que escreve nada no txt, ou seja, vai limpar o txt
        BufferedWriter zerar = new BufferedWriter(new FileWriter(url));
        zerar.write("");
        //fecha o arquivo
        zerar.close();

        for (int i = 0; i < clientes.tamanhoDaLista(); i++) {
            if (clientes.buscarElemento(i).getNome().equals(c.getNome()) && clientes.buscarElemento(i).getIdade() == c.getIdade()) {
                System.out.println(c.getNome() + " excluido com sucesso!");
                continue;
            } else {
                gravaCliente(clientes.buscarElemento(i));
            }
        }
    }

}
