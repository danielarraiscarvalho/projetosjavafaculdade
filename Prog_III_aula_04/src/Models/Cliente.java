package Models;

public class Cliente {
    private String nome;
    private int idade;
    private String endereco;
    private char sexo;
    

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public char getSexo() {
        return sexo;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }
    
    public Cliente(){}

    public Cliente(String nome, int idade, String endereco, char sexo) {
        setEndereco(endereco);
        setNome(nome);
        setIdade(idade);
        setSexo(sexo);
    }
    
    
}
