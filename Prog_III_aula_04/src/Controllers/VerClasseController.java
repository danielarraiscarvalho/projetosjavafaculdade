package Controllers;

import Models.Cliente;
import Models.GerenteArmazenamento;
import Views.TelaCadastro;
import Views.TelaPrincipal;
import Views.VerClientes;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class VerClasseController implements ActionListener {

    private VerClientes tela = null;

    public VerClasseController(VerClientes copia) {
        tela = copia;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == tela.getVrOk()) {
            tela.dispose();
        } else if (e.getSource() == tela.getVrNovo()) {
            tela.dispose();
            TelaCadastro tela = new TelaCadastro();
            tela.setAbrirCadastros(true);
            tela.setVisible(true);
        } else if (e.getSource() == tela.getVrAtualizar()) {
            atualizarTabela();
        } else if (e.getSource() == tela.getVrExcluir()) {
            int linhaSelecionada = tela.getVrTabelaClientes().getSelectedRow();
            Cliente cliente = new Cliente();
            cliente.setNome((String) tela.getVrTabelaClientes().getValueAt(linhaSelecionada, 0));
            cliente.setEndereco((String) tela.getVrTabelaClientes().getValueAt(linhaSelecionada, 1));
            cliente.setIdade((int) tela.getVrTabelaClientes().getValueAt(linhaSelecionada, 2));
            cliente.setSexo((char) tela.getVrTabelaClientes().getValueAt(linhaSelecionada, 3));

            try {
                GerenteArmazenamento.removerCliente(cliente);
            } catch (IOException ex) {
                Logger.getLogger(VerClasseController.class.getName()).log(Level.SEVERE, null, ex);
            }

            atualizarTabela();
        }

    }

    public void atualizarTabela() {
        tela.getPainel().remove(tela.getTableScroll());
        try {
            tela.setTableScroll(new JScrollPane(tela.Tabela()));
        } catch (FileNotFoundException ex) {

        }
        tela.getPainel().add(tela.getTableScroll());
        tela.setVisible(true);
    }

}
