package Views;

import Controllers.CadastroController;
import java.awt.GridLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class TelaCadastro extends JFrame {

    private JTextField vrCampoNome = null;
    private JTextField vrCampoEndereço = null;
    private JComboBox<Integer> vrSelecaoIdade = null;
    private JRadioButton vrMaculino = null;
    private JRadioButton vrFeminino = null;
    private JButton vrVercadastros = null;

    private ButtonGroup agrupador = null;
    private JButton vrBOk = null, vrBCancelar = null;
    private CadastroController cadastroController = null;
    
    private boolean abrirCadastros = false;

    private JLabel erro = null;

    public TelaCadastro() {
        cadastroController = new CadastroController(this);
        setTitle("Cadastro de Clientes");
        setSize(400, 200);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLayout(new GridLayout(6, 2));

        add(new JLabel("Nome:"));
        
        vrCampoNome = new JTextField();
        add(vrCampoNome);

        add(new JLabel("Endereço: "));
        
        vrCampoEndereço = new JTextField();
        add(vrCampoEndereço);

        add(new JLabel("Idade"));
        
        vrSelecaoIdade = new JComboBox();
        for (int i = 0; i < 101; i++) {
            vrSelecaoIdade.addItem(i);
        }
        add(vrSelecaoIdade);

        agrupador = new ButtonGroup();
        vrFeminino = new JRadioButton("Feminino");
        vrMaculino = new JRadioButton("Masculino");
        vrMaculino.setSelected(true);
        agrupador.add(vrFeminino);
        agrupador.add(vrMaculino);
        
        add(vrFeminino);
        add(vrMaculino);

        vrBOk = new JButton("Cadastrar");
        vrBCancelar = new JButton("Cancelar");
        vrVercadastros = new JButton("Ver Cadastros");
        
        vrBOk.addActionListener(cadastroController);
        vrBCancelar.addActionListener(cadastroController);
        vrVercadastros.addActionListener(cadastroController);

        add(vrBOk);
        add(vrBCancelar);
        add(vrVercadastros);
        
        erro = new JLabel();
        add(erro);

        setVisible(true);
    }

    public JLabel getErro() {
        return erro;
    }

    public JButton getVrBOk() {
        return vrBOk;
    }

    public JButton getVrBCancelar() {
        return vrBCancelar;
    }

    public JTextField getVrCampoNome() {
        return vrCampoNome;
    }

    public JTextField getVrCampoEndereço() {
        return vrCampoEndereço;
    }

    public JComboBox<Integer> getVrSelecaoIdade() {
        return vrSelecaoIdade;
    }

    public ButtonGroup getAgrupador() {
        return agrupador;
    }

    public JRadioButton getVrMaculino() {
        return vrMaculino;
    }

    public JRadioButton getVrFeminino() {
        return vrFeminino;
    }

    public JButton getVrVercadastros() {
        return vrVercadastros;
    }

    public boolean isAbrirCadastros() {
        return abrirCadastros;
    }

    public void setAbrirCadastros(boolean abrirCadastros) {
        this.abrirCadastros = abrirCadastros;
    }
}
