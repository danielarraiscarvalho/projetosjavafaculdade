package Views;

import Controllers.PrincipalController;
import java.awt.FlowLayout;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class TelaPrincipal extends JFrame {

    //Atributos da classe
    private JMenuBar barraMenu = null;
    private JMenu menu = null;
    private JMenuItem cadastrar = null;
    private JMenuItem verClientes = null;
    private JMenuItem sair = null;
    
    PrincipalController controller = null;

    public TelaPrincipal() {
        //Configura o comportamento da tela
        setTitle("SisCaC");
        setSize(400, 400);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(new FlowLayout());
        
        //Instância os componentes
        barraMenu = new JMenuBar();
        menu = new JMenu("Arquivo");
        cadastrar= new JMenuItem("Cadastrar clientes");
        verClientes = new JMenuItem("Clientes cadastrados");
        sair = new JMenuItem("Sair");
        
        //Implementa a hierarquia de menus
        setJMenuBar(barraMenu);
        barraMenu.add(menu);
        menu.add(cadastrar);
        menu.add(verClientes);
        menu.addSeparator();
        menu.add(sair);
        
        controller = new PrincipalController(this);
        cadastrar.addActionListener(controller);
        verClientes.addActionListener(controller);
        sair.addActionListener(controller);

        //Torna visível todos os componentes vnculados a tela
        setVisible(true);
    }

    public JMenuBar getBarraMenu() {
        return barraMenu;
    }
    public JMenu getMenu() {
        return menu;
    }

    public JMenuItem getVerClientes() {
        return verClientes;
    }
    
    

    public JMenuItem getSair() {
        return sair;
    }
    
    public JMenuItem getCadastrar() {
        return cadastrar;
    }

    public PrincipalController getController() {
        return controller;
    }
    

}
