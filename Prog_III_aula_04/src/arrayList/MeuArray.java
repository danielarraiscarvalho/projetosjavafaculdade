package arrayList;


public class MeuArray <T> {

    private final int EXTRA = 10;
    private T[] lista = (T[]) new Object[EXTRA];
    private int ultimoIndice = 0;

    public int tamanhoDaLista() {
        return ultimoIndice;
    }

    //METODO PARA ADICIONAR NOME A LISTA
    public void add(T nome) {
        //VERIFICA SE HÁ MEMÓRIA DISPONÍVEL
        if (ultimoIndice == lista.length) {
            //REALOCA LISTA
            realocaLista();
        }
        //ADICIONA NOME À LISTA
        lista[ultimoIndice] = nome;
        ultimoIndice++;
    }

    //MÉTODO PARA REALOCAR LISTA
    private void realocaLista() {
        //CRIA LISTA TEMPORARIA
        T[] temp = (T[])new Object[lista.length + EXTRA];

        //COPIA O CONTEÚDO DE LISTA PARA A LISTA TEMPORÁRIA
        for (int posicao = 0; posicao < lista.length; posicao++) {
            temp[posicao] = lista[posicao];
        }

        //REFERÊNCIA A LISTA TEMPORÁRIA COM "LISTA"
        lista = temp;
    }

    //METODO QUE BUSCAR ELEMENTO
    public T buscarElemento(int indice) {
        if (indice > ultimoIndice) {
            return null;
        }
        return lista[indice];
    }

    //METODO QUE BUSCA ELEMENTO POR CONTEÚDO
    int buscarElemento(T conteudo) {
        for (int i = 0; i < ultimoIndice; i++) {
            if (lista[i].equals(conteudo)) {
                return i;
            }
        }
        return 0;
    }

    //METODO DE REMOVER
    public T remover(int indice) {
        //VARIAVEL EXCLUIDA
        T excluido = null;

        //VERIFICA SE O INDICE EXISTE
        if (indice < 0 || indice >= ultimoIndice) {
            //DISPARAR ERRO DO JAVA INDEXOFOUTEXCEPTION
            throw new ArrayIndexOutOfBoundsException();
        } else {
            //GAUARDA O CONTEUDO DO INDICE A SER EXCLUIDO
            excluido = lista[indice];

            //REALOCA CONTEUDO DO INDICES
            for (int i = indice; i < ultimoIndice - 1; i++) {
                lista[i] = lista[i + 1];
            }

            //DECREMENTA EM 1 O ULTIMO INDICE
            ultimoIndice--;

            //RETORNA O VALOR EXCLUIDO
            return excluido;
        }
    }

    public void add(int indice, T nome) {
        if (indice < 0 && indice > ultimoIndice) {
            System.out.println("Indice inválido!!!");
        } else {
            if (ultimoIndice >= lista.length) {
                realocaLista();
            }
            for (int posicao = ultimoIndice; posicao > indice; posicao--) {
                lista[posicao] = lista[posicao - 1];
            }
            lista[indice] = nome;
            ultimoIndice++;
        }
    }

    //METODO QUE RETORNA O CONTEÚDO DA LISTA
    public void verLista() {
        for (int i = 0; i < ultimoIndice; i++) {
            System.out.print(lista[i]);
            if (i < ultimoIndice - 1) {
                System.out.print(" - ");
            }
        }
        System.out.println("");
    }

}
