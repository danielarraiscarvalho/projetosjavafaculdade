package Embaralhador.Interfaces;
public interface Embaralhador {
    //metodo responsavel por embaralhar uma palavra
    //Ele deverar ser implementado nas classes filhas
    public String embaralha(String palavra);
}
