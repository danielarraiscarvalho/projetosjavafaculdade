package Mecanica;

import Mecanica.Interfaces.MecanicaDoJogo;

public class FabricaMecanica {
    //Método estatico do tipo MecanicaDoJogo que retorna um embaralhador de acordo com o o valor do argumento
    public static MecanicaDoJogo retornaMecanica(int Mecanica) {
        switch (Mecanica) {
            case 1:
                return new MecanicaMorteSubita();
            case 2:
                return new MecanicaMaisAcertos();
            case 3:
                return new MecanicaComVidas();
        }
        return null;
    }

}
