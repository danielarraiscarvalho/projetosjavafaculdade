package RecursosExternos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Random;

public class BancoDePalavras {

    //Atributos da classe
    private String nomeArquivo = null;
    private Random vrSorteio = new Random();
    private ArrayList<String> vrPalavras = new ArrayList<String>();

    //Construtor da classe
    public BancoDePalavras(String arquivo) {
        nomeArquivo = arquivo;
        carregaPalavras();
    }

    //Metodo privado utilizado para sortear uma palavra lida do arquivo
    private void carregaPalavras() {
        try {
            //Preenche o array com as palavras do arquivo
            String linha = "";
            File vrArquivo = new File(getClass().getResource(nomeArquivo).toURI());
            FileReader vrLeitor = new FileReader(vrArquivo);
            BufferedReader vrBufferLeitura = new BufferedReader(vrLeitor);

            while ((linha = vrBufferLeitura.readLine()) != null) {
                vrPalavras.add(linha);
            }

            vrBufferLeitura.close();
            vrLeitor.close();
        } catch (Exception e) {
            System.out.println("Nao foi possivel abrir o arquivo");;
        }
    }

    //Metodo utilizado para sortear uma palavra
    public String sorteiaPalavra() {
        //Sorteia uma palavra
        if (vrPalavras.size() > 0) {
            int posicao = vrSorteio.nextInt(vrPalavras.size());
            return vrPalavras.get(posicao);
        }
        return null;
    }
    //Metodo utilizado para sortear uma palavra
    public String sorteiaPalavra(int tamanho) {
        String Palavra="";
        //Sorteia uma palavra com o tamanho passado com argumento
        do {            
            Palavra = sorteiaPalavra();
        } while (Palavra.length() != tamanho);
        return Palavra;
    }
    public static void main(String[] args) {
        BancoDePalavras b = new BancoDePalavras("verbos.txt");
        System.out.println(b.sorteiaPalavra(1));
    }
}
