
public class Tapioca extends Lanche {

	int totalIngredientes;
	
	Tapioca(int totalIngredientes){
		this.totalIngredientes=totalIngredientes;
	}
	
	@Override
	void modoPreparo() {
		System.out.println("Molhar a fariha e colocar e colocar na chapa,");
		System.out.println("rechear a massa e aquecer");
	}

	@Override
	float precoProduto() {
		
		return totalIngredientes * 10;
	}

}
