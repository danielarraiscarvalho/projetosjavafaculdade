CREATE DATABASE `filaesperta` /*!40100 DEFAULT CHARACTER SET utf8 */;

CREATE TABLE `guiches` (
  `id_guiche` int(10) unsigned NOT NULL,
  `aberto` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_guiche`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `senhas_retiradas` (
  `senha` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `categoria` varchar(1) NOT NULL,
  `chamada` tinyint(1) NOT NULL DEFAULT '0',
  `guiche` int(10) unsigned DEFAULT NULL,
  `horario` timestamp(1) NULL DEFAULT NULL,
  `horario_r` timestamp(1) NULL DEFAULT NULL,
  PRIMARY KEY (`senha`),
  KEY `fk_guiche` (`guiche`),
  CONSTRAINT `fk_guiche` FOREIGN KEY (`guiche`) REFERENCES `guiches` (`id_guiche`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;
