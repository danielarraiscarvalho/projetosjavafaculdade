package models;

public final class Senha {
    private int senha;
    private String categoria;
    private boolean chamada;
    private String horarioInsercao;
    private String horarioChamada;

    public Senha(int senha, String categoria, int chamada, String horarioInsercao, String horarioChamada) {
        this.setSenha(senha);
        this.setCategoria(categoria);
        this.setChamada(chamada != 0);
        this.setHorarioInsercao(horarioInsercao);
        this.setHorarioChamada(horarioChamada);
    }
    
    public Senha(int senha, String categoria) {
        this.setSenha(senha);
        this.setCategoria(categoria);
    }
    
    public int getSenha() {
        return senha;
    }

    public void setSenha(int senha) {
        this.senha = senha;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public boolean isChamada() {
        return chamada;
    }

    public void setChamada(int chamada) {
        this.chamada = chamada!=0;
    }
    
    public void setChamada(boolean chamada) {
        this.chamada = chamada;
    }

    public String getHorarioInsercao() {
        return horarioInsercao;
    }

    public void setHorarioInsercao(String horarioInsercao) {
        this.horarioInsercao = horarioInsercao;
    }

    public String getHorarioChamada() {
        return horarioChamada;
    }

    public void setHorarioChamada(String horarioChamada) {
        this.horarioChamada = horarioChamada;
    }
    
    
    
    
}
