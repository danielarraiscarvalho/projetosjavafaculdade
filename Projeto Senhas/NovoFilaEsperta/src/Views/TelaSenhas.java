
package Views;

public class TelaSenhas extends javax.swing.JFrame {

    public TelaSenhas() {
        initComponents();
        setVisible(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jpnlTracoHorizontal = new javax.swing.JPanel();
        jlblSelfService = new javax.swing.JLabel();
        jlblPedidosProntos = new javax.swing.JLabel();
        jlblSenhaPedido1 = new javax.swing.JLabel();
        jlblSenhaPedido2 = new javax.swing.JLabel();
        jlblSenhaPedido4 = new javax.swing.JLabel();
        jlblSenhaPedido3 = new javax.swing.JLabel();
        jlblSenhaSelf1 = new javax.swing.JLabel();
        jlblSenhaSelf2 = new javax.swing.JLabel();
        jlblSenhaSelf4 = new javax.swing.JLabel();
        jlblSenhaSelf3 = new javax.swing.JLabel();
        jpnlTracoVertical = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(0, 0, 0));
        setEnabled(false);
        setMaximumSize(new java.awt.Dimension(1366, 768));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(0, 0, 0));
        jPanel1.setMinimumSize(new java.awt.Dimension(1366, 780));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        javax.swing.GroupLayout jpnlTracoHorizontalLayout = new javax.swing.GroupLayout(jpnlTracoHorizontal);
        jpnlTracoHorizontal.setLayout(jpnlTracoHorizontalLayout);
        jpnlTracoHorizontalLayout.setHorizontalGroup(
            jpnlTracoHorizontalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1370, Short.MAX_VALUE)
        );
        jpnlTracoHorizontalLayout.setVerticalGroup(
            jpnlTracoHorizontalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );

        jPanel1.add(jpnlTracoHorizontal, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 360, 1370, 10));

        jlblSelfService.setFont(new java.awt.Font("Tahoma", 1, 60)); // NOI18N
        jlblSelfService.setForeground(new java.awt.Color(255, 255, 255));
        jlblSelfService.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jlblSelfService.setText("<html><p align=\"right\">FILA SELF<br/> SERVICE</p></html>");
        jPanel1.add(jlblSelfService, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 350, 320, 330));

        jlblPedidosProntos.setFont(new java.awt.Font("Tahoma", 1, 60)); // NOI18N
        jlblPedidosProntos.setForeground(new java.awt.Color(255, 255, 255));
        jlblPedidosProntos.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jlblPedidosProntos.setText("<html><p align=\"right\">PEDIDOS<br/>PRONTOS</p></html>");
        jPanel1.add(jlblPedidosProntos, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 40, 320, 310));

        jlblSenhaPedido1.setFont(new java.awt.Font("Tahoma", 0, 280)); // NOI18N
        jlblSenhaPedido1.setForeground(new java.awt.Color(255, 255, 255));
        jlblSenhaPedido1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jlblSenhaPedido1.setText("0004");
        jPanel1.add(jlblSenhaPedido1, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 40, 620, 310));

        jlblSenhaPedido2.setFont(new java.awt.Font("Tahoma", 0, 70)); // NOI18N
        jlblSenhaPedido2.setForeground(new java.awt.Color(255, 255, 255));
        jlblSenhaPedido2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jlblSenhaPedido2.setText("0003");
        jPanel1.add(jlblSenhaPedido2, new org.netbeans.lib.awtextra.AbsoluteConstraints(1150, 90, 170, 80));

        jlblSenhaPedido4.setFont(new java.awt.Font("Tahoma", 0, 70)); // NOI18N
        jlblSenhaPedido4.setForeground(new java.awt.Color(255, 255, 255));
        jlblSenhaPedido4.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jlblSenhaPedido4.setText("0001");
        jPanel1.add(jlblSenhaPedido4, new org.netbeans.lib.awtextra.AbsoluteConstraints(1150, 250, 170, 80));

        jlblSenhaPedido3.setFont(new java.awt.Font("Tahoma", 0, 70)); // NOI18N
        jlblSenhaPedido3.setForeground(new java.awt.Color(255, 255, 255));
        jlblSenhaPedido3.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jlblSenhaPedido3.setText("0002");
        jPanel1.add(jlblSenhaPedido3, new org.netbeans.lib.awtextra.AbsoluteConstraints(1150, 180, 170, 64));

        jlblSenhaSelf1.setFont(new java.awt.Font("Tahoma", 0, 280)); // NOI18N
        jlblSenhaSelf1.setForeground(new java.awt.Color(255, 255, 255));
        jlblSenhaSelf1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jlblSenhaSelf1.setText("S004");
        jPanel1.add(jlblSenhaSelf1, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 350, 620, 330));

        jlblSenhaSelf2.setFont(new java.awt.Font("Tahoma", 0, 70)); // NOI18N
        jlblSenhaSelf2.setForeground(new java.awt.Color(255, 255, 255));
        jlblSenhaSelf2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jlblSenhaSelf2.setText("S003");
        jPanel1.add(jlblSenhaSelf2, new org.netbeans.lib.awtextra.AbsoluteConstraints(1150, 390, 170, 82));

        jlblSenhaSelf4.setFont(new java.awt.Font("Tahoma", 0, 70)); // NOI18N
        jlblSenhaSelf4.setForeground(new java.awt.Color(255, 255, 255));
        jlblSenhaSelf4.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jlblSenhaSelf4.setText("S001");
        jPanel1.add(jlblSenhaSelf4, new org.netbeans.lib.awtextra.AbsoluteConstraints(1150, 560, 170, 82));

        jlblSenhaSelf3.setFont(new java.awt.Font("Tahoma", 0, 70)); // NOI18N
        jlblSenhaSelf3.setForeground(new java.awt.Color(255, 255, 255));
        jlblSenhaSelf3.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jlblSenhaSelf3.setText("S002");
        jPanel1.add(jlblSenhaSelf3, new org.netbeans.lib.awtextra.AbsoluteConstraints(1150, 480, 170, 82));

        jpnlTracoVertical.setBackground(new java.awt.Color(255, 0, 0));

        javax.swing.GroupLayout jpnlTracoVerticalLayout = new javax.swing.GroupLayout(jpnlTracoVertical);
        jpnlTracoVertical.setLayout(jpnlTracoVerticalLayout);
        jpnlTracoVerticalLayout.setHorizontalGroup(
            jpnlTracoVerticalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 5, Short.MAX_VALUE)
        );
        jpnlTracoVerticalLayout.setVerticalGroup(
            jpnlTracoVerticalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 560, Short.MAX_VALUE)
        );

        jPanel1.add(jpnlTracoVertical, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 90, 5, -1));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1366, 768));

        setBounds(0, 0, 1382, 807);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel jlblPedidosProntos;
    private javax.swing.JLabel jlblSelfService;
    public javax.swing.JLabel jlblSenhaPedido1;
    public javax.swing.JLabel jlblSenhaPedido2;
    public javax.swing.JLabel jlblSenhaPedido3;
    public javax.swing.JLabel jlblSenhaPedido4;
    public javax.swing.JLabel jlblSenhaSelf1;
    public javax.swing.JLabel jlblSenhaSelf2;
    public javax.swing.JLabel jlblSenhaSelf3;
    public javax.swing.JLabel jlblSenhaSelf4;
    private javax.swing.JPanel jpnlTracoHorizontal;
    private javax.swing.JPanel jpnlTracoVertical;
    // End of variables declaration//GEN-END:variables
}
