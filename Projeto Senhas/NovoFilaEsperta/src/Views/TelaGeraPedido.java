package Views;

import Conector.dbCrud;
import java.sql.SQLException;
import java.util.ArrayList;
import models.Senha;
import Listas.Fila;
import java.applet.*;
import java.awt.*;
import java.io.File;
import java.net.MalformedURLException;
import java.util.logging.*;
import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import java.awt.event.*;
import javax.swing.GroupLayout.Alignment;

public class TelaGeraPedido extends JFrame {

	private static final long serialVersionUID = 1L;

	dbCrud crud = new dbCrud();
	
	TelaSenhas painel = null;

	Fila<String> macarrao = null;
	Fila<String> pedidos = null;
	ArrayList<Senha> senhaEPedidos = null;
	ArrayList<Senha> listaSenhasNaoChamadas = null;
	ArrayList<Senha> pedidosChamados = null;
	ArrayList<Senha> senhasChamadas = null;

	public static final int MONITORSECUNDARIO = 0, MONITORPRIMARIO = 0;

	public TelaGeraPedido() throws SQLException {

		crud.createDB();
		painel = new TelaSenhas();
		initComponents();
		atualizarTabela();
		if (abrirNaTelaSecundaria(MONITORSECUNDARIO, painel)) {
			statusPainelAberto();
		} else {
			statusPainelFechado();
		}
	}

	public static void main(String[] args) throws SQLException {
		new TelaGeraPedido().setVisible(true);
	}

	private void initComponents() {
		jpnlAplicacao = new JPanel();

		jlblNomeSitema = new JLabel();
		jlblNomeSitema.setHorizontalAlignment(SwingConstants.CENTER);
		jlblNomeSitema.setBounds(0, 15, 656, 32);

		Label_hora = new JLabel();
		Label_hora.setBounds(464, 0, 192, 19);

		jscllTabelaSenhas = new JScrollPane();
		jscllTabelaSenhas.setBounds(10, 68, 255, 315);

		tblSenhas = new JTable();

		jpnlSenhas = new JPanel();
		jpnlSenhas.setBounds(282, 248, 364, 86);

		jbtNovaSenha = new JButton();
		jbtNovaSenha.setBounds(16, 37, 171, 31);
		jbtNovaSenha.setBackground(new Color(220, 220, 220));

		jbtProximaSenha = new JButton();
		jbtProximaSenha.setBounds(197, 37, 154, 31);
		jbtProximaSenha.setBackground(new Color(220, 220, 220));

		jpnlPedidos = new JPanel();
		jpnlPedidos.setBounds(282, 58, 364, 184);

		jtfInserirPedido = new JTextField();
		jtfInserirPedido.setBounds(16, 46, 230, 28);

		jtfChamarPedido = new JTextField();
		jtfChamarPedido.setBounds(16, 98, 230, 28);

		jlblPedido = new JLabel();
		jlblPedido.setBounds(16, 26, 230, 14);

		jlblSenha = new JLabel();
		jlblSenha.setBounds(16, 80, 230, 14);

		jbtInserirPedido = new JButton();
		jbtInserirPedido.setBounds(252, 46, 99, 28);
		jbtInserirPedido.setBackground(new Color(220, 220, 220));

		jbtChamarPedido = new JButton();
		jbtChamarPedido.setBounds(252, 98, 99, 28);
		jbtChamarPedido.setBackground(new Color(220, 220, 220));

		jtfProximaSenha = new JButton();
		jtfProximaSenha.setBounds(87, 137, 206, 31);
		jtfProximaSenha.setBackground(new Color(220, 220, 220));

		jbtChamarNovamente = new JButton();
		jbtChamarNovamente.setBounds(282, 338, 364, 45);

		jpnlStatusPainel = new JPanel();
		jpnlStatusPainel.setBounds(10, 400, 255, 50);

		jtbFecharPainel = new JToggleButton();
		jtbFecharPainel.setBounds(98, 16, 137, 23);
		jtbFecharPainel.setBackground(new Color(220, 220, 220));

		jlblStatusPainel = new JLabel();
		jlblStatusPainel.setBounds(10, 20, 80, 14);

		jmbMenu = new JMenuBar();

		Menu_TelaSenha = new JMenu();
		ItemMenuSair = new JMenuItem();

		Menu_Ajuda = new JMenu();

		ItemMenu_Suporte = new JMenuItem();
		ItemMenu_Sobre = new JMenuItem();

		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setTitle("Senhas Pedidos");
		setResizable(false);

		jpnlAplicacao.setForeground(new Color(255, 255, 255));
		jpnlAplicacao.setLayout(null);

		jlblNomeSitema.setFont(new Font("Dialog", 1, 24)); 
		jlblNomeSitema.setText("Sistema Chamada Senhas");

		jpnlAplicacao.add(jlblNomeSitema);
		jpnlAplicacao.add(Label_hora);

		jscllTabelaSenhas.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
		jscllTabelaSenhas.setFont(new Font("Arial", 1, 18)); 

		tblSenhas.setFont(new Font("Arial", 0, 14));
		tblSenhas.setRowSelectionAllowed(false);
		tblSenhas.setSelectionBackground(new Color(204, 204, 204));
		tblSenhas.setSelectionForeground(new Color(153, 255, 153));
		tblSenhas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tblSenhas.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				tblSenhasMouseClicked(evt);
			}
		});

		jscllTabelaSenhas.setViewportView(tblSenhas);

		jpnlAplicacao.add(jscllTabelaSenhas);

		jpnlSenhas.setBorder(BorderFactory.createTitledBorder(null, "Senhas", TitledBorder.DEFAULT_JUSTIFICATION,
				TitledBorder.DEFAULT_POSITION, new Font("Dialog", 1, 18))); // NOI18N
		jpnlSenhas.setPreferredSize(new Dimension(320, 159));

		jbtNovaSenha.setText("GERAR NOVA SENHA");
		jbtNovaSenha.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				jbtNovaSenhaActionPerformed(evt);
			}
		});

		jbtProximaSenha.setText("PR\u00D3XIMA SENHA");
		jbtProximaSenha.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				jbtProximaSenhaActionPerformed(evt);
			}
		});

		jpnlAplicacao.add(jpnlSenhas);
		jpnlSenhas.setLayout(null);
		jpnlSenhas.add(jbtNovaSenha);
		jpnlSenhas.add(jbtProximaSenha);

		jpnlPedidos.setBorder(BorderFactory.createTitledBorder(null, "Pedidos", TitledBorder.DEFAULT_JUSTIFICATION,
				TitledBorder.DEFAULT_POSITION, new Font("Dialog", 1, 18)));
		jpnlPedidos.setPreferredSize(new Dimension(320, 159));

		jtfInserirPedido.setFont(new Font("Tahoma", 0, 18)); 
		jtfInserirPedido.setHorizontalAlignment(JTextField.CENTER);

		jtfChamarPedido.setFont(new Font("Tahoma", 0, 18));
		jtfChamarPedido.setHorizontalAlignment(JTextField.CENTER);

		jlblPedido.setText("INSERIR PEDIDO");

		jlblSenha.setText("CHAMAR PEDIDO");

		jbtInserirPedido.setText("INSERIR");
		jbtInserirPedido.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				jbtInserirPedidoActionPerformed(evt);
			}
		});

		jbtChamarPedido.setText("CHAMAR");
		jbtChamarPedido.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				jbtChamarPedidoActionPerformed(evt);
			}
		});

		jtfProximaSenha.setText("CHAMAR PR\u00D3XIMO PEDIDO");
		jtfProximaSenha.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				jtfProximaSenhaActionPerformed(evt);
			}
		});

		jpnlAplicacao.add(jpnlPedidos);

		jpnlPedidos.setLayout(null);
		jpnlPedidos.add(jtfProximaSenha);
		jpnlPedidos.add(jtfInserirPedido);
		jpnlPedidos.add(jtfChamarPedido);
		jpnlPedidos.add(jlblSenha);
		jpnlPedidos.add(jlblPedido);
		jpnlPedidos.add(jbtChamarPedido);
		jpnlPedidos.add(jbtInserirPedido);

		jbtChamarNovamente.setBackground(new Color(0, 204, 51));
		jbtChamarNovamente.setFont(new Font("Dialog", 1, 18)); // NOI18N
		jbtChamarNovamente.setText("CHAMAR SENHA NOVAMENTE");
		jbtChamarNovamente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				jbtChamarNovamenteActionPerformed(evt);
			}
		});

		jpnlAplicacao.add(jbtChamarNovamente);

		jpnlStatusPainel.setBorder(BorderFactory.createTitledBorder(null, "Painel da TV",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", 1, 11))); // NOI18N
		jpnlStatusPainel.setForeground(new Color(255, 255, 255));

		jtbFecharPainel.setText("Fechar Painel");
		jtbFecharPainel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				jtbFecharPainelActionPerformed(evt);
			}
		});

		jlblStatusPainel.setText("Aberto");

		jpnlAplicacao.add(jpnlStatusPainel);

		jpnlStatusPainel.setLayout(null);
		jpnlStatusPainel.add(jlblStatusPainel);
		jpnlStatusPainel.add(jtbFecharPainel);

		Menu_TelaSenha.setText("Ferramentas");

		ItemMenuSair.setText("Sair");
		ItemMenuSair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				ItemMenuSairActionPerformed(evt);
			}
		});
		Menu_TelaSenha.add(ItemMenuSair);

		jmbMenu.add(Menu_TelaSenha);

		Menu_Ajuda.setText("Ajuda");

		ItemMenu_Suporte.setText("Suporte");
		ItemMenu_Suporte.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				ItemMenu_SuporteActionPerformed(evt);
			}
		});
		Menu_Ajuda.add(ItemMenu_Suporte);

		ItemMenu_Sobre.setText("Sobre");
		ItemMenu_Sobre.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				ItemMenu_SobreActionPerformed(evt);
			}
		});
		Menu_Ajuda.add(ItemMenu_Sobre);

		jmbMenu.add(Menu_Ajuda);

		setJMenuBar(jmbMenu);

		GroupLayout layout = new GroupLayout(getContentPane());
		layout.setHorizontalGroup(
			layout.createParallelGroup(Alignment.TRAILING)
				.addGroup(Alignment.LEADING, layout.createSequentialGroup()
					.addGap(1)
					.addComponent(jpnlAplicacao, GroupLayout.DEFAULT_SIZE, 658, Short.MAX_VALUE))
		);
		layout.setVerticalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addGap(1)
					.addComponent(jpnlAplicacao, GroupLayout.DEFAULT_SIZE, 462, Short.MAX_VALUE))
		);
		getContentPane().setLayout(layout);

		pack();
		setLocationRelativeTo(null);
	}

	private void ItemMenuSairActionPerformed(ActionEvent evt) {
		int opcao = JOptionPane.showConfirmDialog(null, "Deseja realmente sair do sistema?");
		if (opcao == 0) {
			System.exit(0);
		}
	}

	private void ItemMenu_SuporteActionPerformed(ActionEvent evt) {
		new TelaSuporte().setVisible(true);
	}

	private void ItemMenu_SobreActionPerformed(ActionEvent evt) {
		new Sobre().setVisible(true);
	}

	private void jtbFecharPainelActionPerformed(ActionEvent evt) {
		if (jtbFecharPainel.getModel().isSelected()) {
			statusPainelAberto();

		} else {
			statusPainelFechado();
		}
	}

	private void jbtChamarNovamenteActionPerformed(ActionEvent evt) {
		tocarAlerta();
	}

	private void jtfProximaSenhaActionPerformed(ActionEvent evt) {

	}

	private void jbtChamarPedidoActionPerformed(ActionEvent evt) {
		Senha senha = null;
		try {
			senha = new Senha(Integer.valueOf(jtfChamarPedido.getText().toString()).intValue(), "P");
		} catch (Exception e) {
		}
		boolean temSenha = false;
		if (senha != null) {
			for (int i = 0; i < pedidos.size(); i++) {
				if (pedidos.get(i).equalsIgnoreCase(senha.getSenha() + "")) {
					temSenha = true;
				}
			}
			for (int i = 0; i < pedidosChamados.size(); i++) {
				String s = pedidosChamados.get(i).getSenha() + "";
				if (s.equalsIgnoreCase(senha.getSenha() + "")) {
					temSenha = true;
				}
			}
			if (temSenha) {
				try {
					crud.chamarSenha(senha);
					atualizarTabela();
					atualizarPainel();
				} catch (SQLException ex) {
				}
				tocarAlerta();
			} else {
				JOptionPane.showMessageDialog(null, "Pedido não encontrado!!!");
			}
		}
		jtfChamarPedido.setText("");
	}

	private void jbtInserirPedidoActionPerformed(ActionEvent evt) {
		Senha senha = null;
		boolean temSenha = false;
		try {
			senha = new Senha(Integer.valueOf(jtfInserirPedido.getText().toString()).intValue(), "P");
		} catch (Exception e) {
		}
		if (senha != null) {
			for (int i = 0; i < pedidos.size(); i++) {
				if (pedidos.get(i).equalsIgnoreCase(senha.getSenha() + "")) {
					temSenha = true;
				}
			}
			if (!temSenha) {
				try {
					crud.inserirPedido(senha);
					atualizarTabela();
				} catch (Exception e) {
				}
			} else {
				JOptionPane.showMessageDialog(null, "Esse pedido já foi está cadastrado!");
			}
		}
		jtfInserirPedido.setText("");
	}

	// EVENTOS DOS BOTOES, PODE COLOCAR CADA PARTE DENTRO
	private void jbtProximaSenhaActionPerformed(ActionEvent evt) {
		if (macarrao.size() > 0) {
			Senha temp = new Senha(Integer.valueOf(macarrao.remover()), "M");
			try {
				crud.chamarSenha(temp);
			} catch (SQLException ex) {
				Logger.getLogger(TelaGeraPedido.class.getName()).log(Level.SEVERE, null, ex);
			}
			try {
				atualizarTabela();
			} catch (SQLException ex) {
				Logger.getLogger(TelaGeraPedido.class.getName()).log(Level.SEVERE, null, ex);
			}
			tocarAlerta();
		}
	}

	private void jbtNovaSenhaActionPerformed(ActionEvent evt) {
		try {
			crud.inserirPedido(new Senha(senhasChamadas.size() + macarrao.size() + 1, "M"));
		} catch (SQLException ex) {
		}
		try {
			atualizarTabela();
		} catch (SQLException ex) {
			Logger.getLogger(TelaGeraPedido.class.getName()).log(Level.SEVERE, null, ex);
		}

	}// GEN-LAST:event_jbtNovaSenhaActionPerformed

	private void tblSenhasMouseClicked(MouseEvent evt) {
		int nLinha = tblSenhas.getSelectedRow();
		int nCelula = tblSenhas.getSelectedColumn();
		String senha = "";
		try {
			senha = tblSenhas.getValueAt(nLinha, nCelula).toString();
		} catch (Exception e) {
		}

		if (senha != null && !senha.equalsIgnoreCase("") && nCelula == 0) {
			jtfChamarPedido.setText(senha);
		} else {
			jtfChamarPedido.setText("");
		}
	}

	
	private JMenuItem ItemMenuSair;
	private JMenuItem ItemMenu_Sobre;
	private JMenuItem ItemMenu_Suporte;

	private JMenu Menu_Ajuda;
	private JMenu Menu_TelaSenha;
	private JLabel Label_hora;
	private JLabel jlblPedido;
	private JLabel jlblNomeSitema;
	private JLabel jlblSenha;
	public JLabel jlblStatusPainel;

	private JPanel jpnlSenhas;
	private JPanel jpnlPedidos;
	private JPanel jpnlAplicacao;
	private JPanel jpnlStatusPainel;

	private JMenuBar jmbMenu;

	private JButton jbtChamarNovamente;
	private JButton jbtChamarPedido;
	private JButton jbtInserirPedido;
	private JButton jbtNovaSenha;
	private JButton jbtProximaSenha;
	private JButton jtfProximaSenha;

	private JScrollPane jscllTabelaSenhas;

	public JToggleButton jtbFecharPainel;

	private JTextField jtfChamarPedido;
	private JTextField jtfInserirPedido;

	private JTable tblSenhas;

	public void atualizarTabela() throws SQLException {
		macarrao = new Fila();
		pedidos = new Fila();
		senhaEPedidos = new ArrayList<>();

		listaSenhasNaoChamadas = new ArrayList<>();

		pedidosChamados = new ArrayList<>();
		senhasChamadas = new ArrayList<>();

		senhaEPedidos = crud.senhaEPedidos();
		listaSenhasNaoChamadas = crud.senhasNaoChamadas();
		for (int i = 0; i < listaSenhasNaoChamadas.size(); i++) {
			Senha senha = listaSenhasNaoChamadas.get(i);
			if (senha.getCategoria().equalsIgnoreCase("M")) {
				senhasChamadas.add(senha);
			} else if (senha.getCategoria().equalsIgnoreCase("p")) {
				pedidosChamados.add(senha);
			}
		}

		for (int i = 0; i < senhaEPedidos.size(); i++) {
			Senha senha = senhaEPedidos.get(i);
			if (senha.getCategoria().equalsIgnoreCase("m") && !senha.isChamada()) {
				macarrao.inserir(senha.getSenha() + "");
			} else if (senha.getCategoria().equalsIgnoreCase("p") && !senha.isChamada()) {
				pedidos.inserir(senha.getSenha() + "");
			}
		}
		atualizarPainel();
		int maior = 0;
		if (macarrao.size() > pedidos.size()) {
			maior = macarrao.size();
		} else {
			maior = pedidos.size();
		}

		String[][] matriz = new String[maior][2];
		for (int k = 0; k < maior; k++) {
			matriz[k] = new String[] { pedidos.get(k), macarrao.get(k) };
		}

		DefaultTableModel modelo = (DefaultTableModel) tblSenhas.getModel();
		modelo.setDataVector(matriz, new String[] { "Pedidos", "SELF SERVICE" });
		tblSenhas.setModel(modelo);
	}

	public void tocarAlerta() {
		String bip = System.getenv("ProgramFiles") + "\\FilaEsperta\\AUDIO\\som.wav";
		try {
			AudioClip clip = Applet.newAudioClip(new File(bip).toURL());
			clip.play();
		} catch (MalformedURLException ex) {
			System.out.println("erro ao tocar audio!");
		}
	}

	public void atualizarPainel() {
		if (pedidosChamados != null) {
			if (pedidosChamados.size() > 0) {
				painel.jlblSenhaPedido1
						.setText(zeroFill(pedidosChamados.get(pedidosChamados.size() - 1).getSenha(), 4));
			}
			if (pedidosChamados.size() > 1) {
				painel.jlblSenhaPedido2
						.setText(zeroFill(pedidosChamados.get(pedidosChamados.size() - 2).getSenha(), 4));
			}
			if (pedidosChamados.size() > 2) {
				painel.jlblSenhaPedido3
						.setText(zeroFill(pedidosChamados.get(pedidosChamados.size() - 3).getSenha(), 4));
			}
			if (pedidosChamados.size() > 3) {
				painel.jlblSenhaPedido4
						.setText(zeroFill(pedidosChamados.get(pedidosChamados.size() - 4).getSenha(), 4));
			}

			if (senhasChamadas.size() > 0) {
				painel.jlblSenhaSelf1.setText(zeroFill(senhasChamadas.get(senhasChamadas.size() - 1).getSenha(), 4));
			}
			if (senhasChamadas.size() > 1) {
				painel.jlblSenhaSelf2.setText(zeroFill(senhasChamadas.get(senhasChamadas.size() - 2).getSenha(), 4));
			}
			if (senhasChamadas.size() > 2) {
				painel.jlblSenhaSelf3.setText(zeroFill(senhasChamadas.get(senhasChamadas.size() - 3).getSenha(), 4));
			}
			if (senhasChamadas.size() > 3) {
				painel.jlblSenhaSelf4.setText(zeroFill(senhasChamadas.get(senhasChamadas.size() - 4).getSenha(), 4));
			}
		}
	}

	public String zeroFill(int numero, int qDigitos) {
		String numeroNovo = numero + "";
		while (numeroNovo.length() != qDigitos) {
			numeroNovo = "0" + numeroNovo;
		}
		return numeroNovo;
	}

	public void statusPainelFechado() {
		TitledBorder borda = BorderFactory.createTitledBorder(null, "Painel da TV", TitledBorder.DEFAULT_JUSTIFICATION,
		TitledBorder.DEFAULT_POSITION, new Font("Tahoma", 1, 11), Color.WHITE);
		jlblStatusPainel.setText("Fechado");
		jtbFecharPainel.setText("Abrir Painel");
		jlblStatusPainel.setForeground(Color.white);
		borda.setTitleColor(Color.white);
		jpnlStatusPainel.setBorder(borda);
		jpnlStatusPainel.setBackground(Color.red);
		painel.dispose();
	}

	public void statusPainelAberto() {
		if (abrirNaTelaSecundaria(MONITORSECUNDARIO, painel)) {
			TitledBorder borda = BorderFactory.createTitledBorder(null, "Painel da TV",
					TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", 1, 11),
					Color.WHITE);
			jlblStatusPainel.setText("Aberto");
			jtbFecharPainel.setText("Fechar Painel");
			jlblStatusPainel.setForeground(Color.black);
			borda.setTitleColor(Color.black);
			jtbFecharPainel.setSelected(true);
			jpnlStatusPainel.setBorder(borda);
			jpnlStatusPainel.setBackground(new Color(0xF0F0F0));
		} else {
			jtbFecharPainel.setSelected(false);
			jlblStatusPainel.setText("Fechado");
			jtbFecharPainel.setText("Abrir Painel");
		}
	}

	public boolean abrirNaTelaSecundaria(int tela, JFrame janela) {
		GraphicsEnvironment environment = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice[] device = environment.getScreenDevices();
		if (tela > device.length - 1 || tela < device.length - 1) {
			JOptionPane.showMessageDialog(null, "Não há uma segunda tela para exibir o painel!");
			return false;
		} else {
			janela.dispose();
			janela.setUndecorated(true);
			janela.setLocation(device[tela].getDefaultConfiguration().getBounds().x, painel.getY());
			System.out.println(device.length);
			janela.setExtendedState(MAXIMIZED_BOTH);
			janela.setVisible(true);
			return true;
		}
	}
}
