
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class Imc extends JFrame implements ActionListener, MouseListener {

    JTextField altura = null;
    JTextField peso = null;

    JButton calcular = null;

    JLabel alturaL = new JLabel("Altura: ");
    JLabel pesoL = new JLabel("Peso: ");
    JLabel mouse = new JLabel("fsdfsdf");

    public Imc() {
        setTitle("Calcular IMC");
        setSize(400, 400);
        setLayout(new FlowLayout());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        altura = new JTextField(30);
        peso = new JTextField(30);
        //altura.setSize(40);
        //peso.setSize(40);
        calcular = new JButton("Calcular");

        add(alturaL);
        add(altura);

        add(pesoL);
        add(peso);

        add(calcular);
        add(mouse);

        setVisible(true);
        
        

        calcular.addActionListener(this);

    }

    public String calcular(float imc) {
        if (imc < 16.00f) {
            return "baixo peso muito grave";
        } else if (imc <= 16.99f) {
            return "baixo peso grave";
        } else if (imc <= 18.49f) {
            return "baixo peso";
        } else if (imc <= 24.99f) {
            return "peso normal";
        } else if (imc <= 29.99f) {
            return "sobrepeso";
        } else if (imc <= 34.99f) {
            return "obesidade grau I";
        } else if (imc <= 39.99f) {
            return "obesidade grau II";
        } else if (imc > 40f) {
            return "obesidade grau III(obesidade m�rbida)";
        } else {
            return "acima de todos os índices";
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        float alturaInt = Float.valueOf(altura.getText());
        float pesoInt = Float.valueOf(peso.getText());

        float imc = (pesoInt / (alturaInt * alturaInt));

        String mensagem = String.format("Seu IMC é %.2f e você está com %s", imc, calcular(imc));
        JOptionPane.showMessageDialog(this, mensagem);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        mouse.setText("Mouse clicado!!!");
    }

    @Override
    public void mousePressed(MouseEvent e) {
        mouse.setText("Mouse pressionado!!!");
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        mouse.setText("Mouse Released!!!");//To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        mouse.setText("Mouse na área!!!");
    }

    @Override
    public void mouseExited(MouseEvent e) {
        mouse.setText("Mouse saiu"); //To change body of generated methods, choose Tools | Templates.
    }
}
