import java.util.Random;
import java.util.Scanner;
public class NovoClass {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        Random aleatorio = new Random();
        int opcao = 0;
        //Variáveis para uso na ordenação do vetor e da matriz
        int auxOrdenacao = 0, k = 0;
        int[] vector = new int[10];
        int[] vetorAuxDeOrdDaMatriz = new int[20];
        int[][] tabela = new int[4][5];
        boolean vetorPreenchido = false;
        boolean matrizPreenchida = false;

        while (opcao != 7) {
            System.out.println("Menu:");
            System.out.println("1 - Preencher vetor");
            System.out.println("2 - Ordenar vetor");
            System.out.println("3 - Imprimir vetor");
            System.out.println("4 - Preencher matriz");
            System.out.println("5 - Ordenar matriz");
            System.out.println("6 - Imprimir matriz");
            System.out.println("7 - Sair");
            opcao = s.nextInt();

            if (!(opcao > 0 && opcao < 8)) {
                System.out.println("                       Opção Inválida! Tente novamente!");
                opcao = s.nextInt();
            }
            switch (opcao) {
                case 1:
                    for (int i = 0; i < 100; ++i) {
                        System.out.println();
                    }
                    for (int i = 0; i < 10; i++) {
                        vector[i] = aleatorio.nextInt(100);
                    }
                    vetorPreenchido = true;
                    break;
                case 2:
                    for (int i = 0; i < 100; ++i) {
                        System.out.println();
                    }
                    if (vetorPreenchido) {
                        for (int i = 0; i < 10; i++) {
                            for (int j = 0; j < 10; j++) {
                                if (vector[i] < vector[j]) {
                                    auxOrdenacao = vector[i];
                                    vector[i] = vector[j];
                                    vector[j] = auxOrdenacao;
                                }
                            }
                        }
                    } else {
                        System.out.println("                                Vetor não preenchido!!");
                    }
                    break;
                case 3:
                    for (int i = 0; i < 100; ++i) {
                        System.out.println();
                    }
                    if (vetorPreenchido) {
                        System.out.print("Vetor: ");
                        for (int i = 0; i < 10; i++) {
                            System.out.print("[" + vector[i] + "]");
                        }
                        System.out.println();
                    } else {
                        System.out.println("                         Vetor não preenchido!!");
                    }
                    break;
                case 4:
                    for (int i = 0; i < 100; ++i) {
                        System.out.println();
                    }
                    for (int i = 0; i < 4; i++) {
                        for (int j = 0; j < 5; j++) {
                            tabela[i][j] = aleatorio.nextInt(100);
                            vetorAuxDeOrdDaMatriz[k] = tabela[i][j];
                            k++;
                        }
                    }
                    matrizPreenchida = true;
                    System.out.println("                       Matriz preenchida com SUCESSO!!");
                    break;
                case 5:
                    for (int i = 0; i < 100; ++i) {
                        System.out.println();
                    }
                    if (matrizPreenchida) {
                        for (int i = 0; i < 20; i++) {
                            for (int j = 0; j < 20; j++) {
                                if (vetorAuxDeOrdDaMatriz[i] < vetorAuxDeOrdDaMatriz[j]) {
                                    auxOrdenacao = vetorAuxDeOrdDaMatriz[i];
                                    vetorAuxDeOrdDaMatriz[i] = vetorAuxDeOrdDaMatriz[j];
                                    vetorAuxDeOrdDaMatriz[j] = auxOrdenacao;
                                }
                            }
                        }
                        k = 0;
                        for (int i = 0; i < 4; i++) {
                            for (int j = 0; j < 5; j++) {
                                tabela[i][j] = vetorAuxDeOrdDaMatriz[k];
                                k++;
                            }
                        }
                    } else {
                        System.out.println("                      Matriz não preenchida!!");
                    }
                    break;
                case 6:
                    for (int i = 0; i < 100; ++i) {
                        System.out.println();
                    }
                    if (matrizPreenchida) {
                        System.out.println();
                        for (int i = 0; i < 4; i++) {
                            for (int j = 0; j < 5; j++) {
                                System.out.print("[" + tabela[i][j] + "]");
                            }
                            System.out.println();
                        }
                    } else {
                        System.out.println("Matriz não preenchida!!");
                    }
                    break;
            }
        }
        for (int i = 0; i < 100; ++i) {
            System.out.println();
        }
    }
}