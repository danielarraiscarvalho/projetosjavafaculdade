import java.io.IOException;
import java.util.ArrayList;

public class Bot {
	boolean comecouJogo;
	boolean repostaInvalida;

	boolean errou;
	ClienteThread copia;
	int perguntaQuiz = 0;
	int perguntaRecepcao = 0;

	String nome = "Jarbes";
	String nomeCliente;
	String valorDoCampo;

	ArrayList<Pergunta> perguntasQuiz = new ArrayList<>();
	ArrayList<String> perguntasAbertura = new ArrayList<>();

	String pa1 = "Oi,\nMeu nome � Jarbes, e o seu?";

	Pergunta p1 = new Pergunta("quem era o homem mais sedutor do mundo?", "DOM JUAN",
			new String[] { "DOM ANT�NIO", "DOM MARCO", "DOM CARLOS" });
	Pergunta p2 = new Pergunta("de quantos anos � constitu�do um s�culo?", "100",
			new String[] { "1000", "800", "1500" });
	Pergunta p3 = new Pergunta("qual � o coletivo de c�es?", "MATILHA",
			new String[] { "REBANHO", "CARDUME", "MANADA" });
	Pergunta p4 = new Pergunta("segundo a B�blia, em que rio Jesus foi batizado por Jo�o Batista?", "NO RIO JORD�O",
			new String[] { "NO RIO NILO", "NO RIO SENA", "NO RIO RENO" });
	Pergunta p5 = new Pergunta("Qual � a maior floresta do planeta?", "AMAZ�NICA",
			new String[] { "DA TIJUCA", "DE SHERWOOD", "NEGRA" });

	public Bot(ClienteThread cliente) {
		copia = cliente;
		perguntasQuiz.add(p1);
		perguntasQuiz.add(p2);
		perguntasQuiz.add(p3);
		perguntasQuiz.add(p4);
		perguntasQuiz.add(p5);
		perguntasAbertura.add(pa1);
	}

	public void perguntar() {
		if (!comecouJogo && !repostaInvalida) {
			String p1 = perguntasAbertura.get(perguntaRecepcao);
			botName();
			copia.enviaMensagem(p1);
			perguntaRecepcao += 1;
		} else if (comecouJogo && !errou) {
			Pergunta p = perguntasQuiz.get(perguntaQuiz);
			botName();
			copia.enviaMensagem(nomeCliente + " " + p.getPergunta());
		}

	}

	public String Responder(String resposta) throws IOException {
		if (!comecouJogo) {
			recepcao(resposta);
		} else if (comecouJogo) {
			jogar(resposta);
		}
		return resposta;
	}

	public void elogios() {
		switch (perguntaQuiz) {
		case 1:
			copia.enviaMensagem("Olha s� eu nunca duvidei de sua capacidade!");
			break;
		default:
			break;
		}
	}

	public void botName() {
		copia.enviaMensagem("========================== " + nome + " ========================");
	}

	public void jogar(String resposta) {
		Pergunta p = perguntasQuiz.get(perguntaQuiz);
		int i = -1;
		try {
			i = Integer.valueOf(resposta);
		} catch (Exception e) {
		}

		if (i > 4 || i == -1) {
			copia.enviaMensagem("N�o sei lidar com essa resposta!");
			copia.enviaMensagem("Voc� deve informar um n�mero equivalente a uma das\nalternativas dispon�veis!!!");
		} else if ((p.alternativas.get(i - 1).equals(p.resposta))) {
			copia.respostaCliente();
			perguntaQuiz++;
			botName();
			copia.enviaMensagem("Correto");
				if (perguntaQuiz>4) {
					copia.enviaMensagem("Parab�ns " + nomeCliente
						+ " voc� conseguiu acertar todas\n Voc� ganhou os 6% de desconto!\nTenha um bom resto de dia, e s� lembrando que \nem breve um atendente entrar� em contato com voc�.");
				}
		} else if (!(p.alternativas.get(i - 1).equals(p.resposta))) {
			botName();
			errou = true;
			copia.enviaMensagem(nomeCliente + " que pena! voc� errou, ent�o s� vai levar 3% de desconto \n resposta correta �: " + perguntaQuiz+1 + ") "
					+ p.resposta + "\nParamos por aqui! At� a pr�xima!");
		}

	}

	public void recepcao(String resposta) {
		switch (perguntaRecepcao) {
		case 0:
			copia.respostaCliente();
			break;
		case 1:
			if (copia.novaMensagem.equals("")) {
				repostaInvalida = true;
				botName();
				copia.enviaMensagem("Por favor, pe�o que informe um nome, sem ele n�o poderemos continuar!");
			} else if (nomeCliente == null) {
				repostaInvalida = false;
				nomeCliente = copia.novaMensagem;
				copia.respostaCliente();
				copia.novaMensagem = "";

				perguntasAbertura.add(1, nomeCliente
						+ " obrigado! Ei poderia tamb�m me informar o numero do seu CPF?\n(apenas o CPF) Para mim poder registrar e realizar o atendimento.");

				perguntasAbertura.add(2, "Obrigado, " + nomeCliente
						+ "! S� que, � o seguinte, eu sou um bot de CS GO que trabalha aqui\n "
						+ "temporariamente suprindo a falta de humanos. Ent�o para ser sincero \n"
						+ "eu s� posso lhe ajudar guardando aqui no meu banco de dados sua necessidade,"
						+ "\npara depois eu repass�-la aos atendentes e eles lhe retornarem assim\n"
						+ "que houver disponibilidade. O que voc� acha, assim est� bom? se sim, pode falar sua necessidade.");

				perguntasAbertura.add(3,
						nomeCliente + " mais uma vez obrigado. Mas antes de voc� ir queria que voc�\n"
								+ "participasse de um desafio - um quiz. Em troca, s� por participar,\n"
								+ "voc� vai ganhar 3% de desconto em sua pr�xima compra, e se voc� acertar\n"
								+ "todas, ganha mais 3%. Iai topa? sim/n�o");
			}

			break;
		case 2:
			if (copia.novaMensagem.equals("")) {
				repostaInvalida = true;
				botName();
				copia.enviaMensagem("Por favor, pe�o que informe um CPF valido!!");
			} else {
				repostaInvalida = false;
				copia.respostaCliente();
			}
			break;
		case 3:
			copia.respostaCliente();
			break;
		case 4:
			if ("sim".equalsIgnoreCase(resposta)) {
				copia.respostaCliente();
				perguntaRecepcao++;
				botName();
				copia.enviaMensagem(nomeCliente + " pronto para Come�ar? mas antes vamos conhecer as regras!"
						+ "\n    1� Voc� j� tem o desconto de 3%;"
						+ "\n    2� Para ganhar os outros tr�s voc� deve acertar todas(5) as respostas;"
						+ "\n    3� Para sair do Jogo basta errar a qualquer momento"
						+ "\n    4� Ao errar uma resposta, o desafio se encerrar e eu saio de cena.");
			} else if ("n�o".equalsIgnoreCase(resposta)) {
				botName();
				copia.enviaMensagem(nomeCliente + " que pena! voc� n�o que jogar? ent�o s� vai levar 3% de desconto"+
					"\nParamos por aqui! Tenha um bom resto de dia e at� a pr�xima \nAt� a pr�xima! vou ali matar uns terror.");
			}else{
				botName();
				copia.enviaMensagem("N�o sei lidar com essa resposta! responda com \"sim\" ou \"n�o\"");
			}
			break;
		case 5:
			copia.respostaCliente();
			if ("sim".equalsIgnoreCase(resposta)) {
				comecouJogo = true;
				botName();
				copia.enviaMensagem(nomeCliente + " ent�o vamos conme�ar!!!");
			}else if ("n�o".equalsIgnoreCase(resposta)) {
				botName();
				copia.enviaMensagem(nomeCliente + " que pena! voc� n�o que jogar? ent�o s� vai levar 3% de desconto"+
					"\nParamos por aqui! Tenha um bom resto de dia e at� a pr�xima \nAt� a pr�xima! vou ali matar uns terror.");
			}else{
				botName();
				copia.enviaMensagem("N�o sei lidar com essa resposta! responda com \"sim\" ou \"n�o\"");
			}
			break;
		default:
			break;
		}
	}
}
