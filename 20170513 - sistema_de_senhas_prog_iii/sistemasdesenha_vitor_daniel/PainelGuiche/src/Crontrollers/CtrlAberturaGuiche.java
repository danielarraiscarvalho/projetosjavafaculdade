package Crontrollers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import Views.PainelAberturaGuiche;
import Views.PainelGuiche;

public class CtrlAberturaGuiche implements ActionListener {

	private PainelAberturaGuiche tela = null;

	public CtrlAberturaGuiche(PainelAberturaGuiche copiaPainelAberturaGuiche) {
		tela = copiaPainelAberturaGuiche;
	}

	@Override
	public void actionPerformed(ActionEvent acao) {
		try {
			tela.valorID = Integer.valueOf(tela.getGuicheAberto().getText());
			System.out.println(tela.valorID);
			tela.getLblAlerta().setVisible(false);
		} catch (Exception e) {
			tela.getLblAlerta().setVisible(true);
		}
	}
}
