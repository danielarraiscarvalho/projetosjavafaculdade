
import pilha.Pilha;
import arrayList.MeuArrais;
import fila.Fila;
import java.util.ArrayList;

public class Principal {

    public static void main(String[] args) {
//        MeuArrais<String> minhaLista = new MeuArrais<String>();
//        ArrayList<String> listaJava = new ArrayList<String>();

        Fila<Integer> minhaFila = new Fila<Integer>();

        Pilha<Integer> minhaPilha = new Pilha<Integer>();

        minhaFila.add(1);
        minhaFila.add(2);
        minhaFila.add(3);
        minhaFila.add(4);
        minhaFila.add(5);
        minhaFila.add(6);
        minhaFila.verLista();
        System.out.println(minhaFila.remover());
        System.out.println(minhaFila.remover());
        System.out.println(minhaFila.remover());
        System.out.println(minhaFila.remover());
        System.out.println(minhaFila.remover());
        System.out.println(minhaFila.remover());

        System.out.println("=======================================");

        minhaPilha.add(1);
        minhaPilha.add(2);
        minhaPilha.add(3);
        minhaPilha.add(4);
        minhaPilha.add(5);
        minhaPilha.add(6);
        minhaPilha.verLista();
        System.out.println(minhaPilha.remover());
        System.out.println(minhaPilha.remover());
        System.out.println(minhaPilha.remover());
        System.out.println(minhaPilha.remover());
        System.out.println(minhaPilha.remover());

//        minhaLista.add("Biscoito Mabel");
//        minhaLista.add("Sorvete");
//        minhaLista.add("Feijão");
//        minhaLista.add(0, "Macarrão");
//        minhaLista.remover(2);
//        minhaLista.add(minhaLista.tamanhoDaLista(), "Pipoca");
//        //minhaLista.remover(10);
//
//        listaJava.add("Biscoito Mabel");
//        listaJava.add("Sorvete");
//        listaJava.add("Feijão");
//        listaJava.add(0, "Macarrão");
//        listaJava.remove(2);
//        listaJava.add(listaJava.size(), "Pipoca");
//        //listaJava.remove(10);
//
//        minhaLista.verLista();
//        System.out.println(listaJava);
//
//        carrinho.add("Feijão");
//        carrinho.add("Feijão");
//        carrinho.add("Feijão");
//        carrinho.add("Feijão");
//        carrinho.add("Feijão");
//        carrinho.add("Feijão");
//        carrinho.add("Feijão");
//        
//        carrinho.verLista();
//        
//        //System.out.println(carrinho.buscarElemento(2));
//        
////        carrinho.verLista();
////        System.out.println(carrinho.remover(2)+" excluido!");
//
//        carrinho.inserir(2, "Arroz");
//        carrinho.verLista();
//        
//        carrinho.inserir(0, "Toddy");
//        carrinho.verLista();
//        
//        carrinho.inserir(12, "Nescau 3.0");
//        carrinho.verLista();
//        
//        carrinho.inserir(12, "Nescau 3.0");
//        carrinho.verLista();
    }
}
